__author__ = 'Mark'

import os

# The expected application exit codes
exit_codes = {
    "0": "Successful execution",
    "1": "Cannot find the given url resource",
    "2": "Cannot overwrite an existing file"
}

def remove_existing_file(file, ignore=False):  # {
    """
    Checks if a file exists and if so removes said file.
    """

    if os.path.isfile(file):  # {
        if not ignore:  # {
            c = input("{f} found. Would you like to remove it? (y/n)\n{p}".format(f=file, p=prompt))
            if c == "y":  # {
                os.remove(file)
                print("The file was removed!")
            # }
            else:  # {
                exit_program(2, IOError("{f} cannot be overwritten".format(f=file)))
            # }
        # }
        elif ignore:  # {
            os.remove(file)
        # }
    # }
# }

def exit_program(code, error):  # {
    """
    Handles exceptions when the program needs to prematurely exit.
    """

    print("[{e}] {m}.\nExiting with code {c}"
          .format(e=error, m=exit_codes[str(code)], c=code))

    exit(code)
# }
