from urllib import request as req
import helper as h

class FileFetcher:
    def __init__(self, day, month, start_year, end_year, stock):  # {
        self.__URL_BASE = "http://ichart.finance.yahoo.com/table.csv?s="
        self.__URL_END = "&ignore=.csv"
        self.day = day
        self.month = month
        self.startYear = start_year
        self.endYear = end_year
        self.stock = stock
        self.full_file = "{}-full.csv".format(self.stock)
    # }

    def get_url_date_string(self):  # {
        """
        Formats the date segment of the url
        :rtypt : A string with the given months
        """

        return "&d={0}&e={1}&f={2}&a={0}&b={1}&c={3}&g=d"\
            .format(self.month - 1, self.day, self.startYear, self.endYear)
    # }

    def get_formatted_url_string(self):  # {
        """
        Formats the url and places each url segment appropriately.
        :rtype : The entire url used to retrieve stocks file.
        """

        return "{base}{stock}{date_string}{end}"\
            .format(base=self.__URL_BASE, stock=self.stock, date_string=self.get_url_date_string(), end=self.__URL_END)
    # }

    def retrieve_file(self):  # {
        """Downloads the stocks file using a given url."""

        # remove_existing_file(temp_file, True)

        try:  # {
            req.urlretrieve(self.get_formatted_url_string(), self.full_file)
        # }
        except IOError as ioe:  # {
            h.exit_program(1, ioe)
            exit(1)
        # }
    # }

    def get_full_file(self):  # {
        """Returns the full_file path for the saved file."""

        return self.full_file
    # }
# }
