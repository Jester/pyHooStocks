__author__ = 'Mark Provenzano'

from os import remove as rm
from os import path

def line_count(file):  # {
    """
    Returns the number of lines contained in the stock file.
    :rtype : int value
    """

    with open(file) as stock_file:  # {
        for i, l in enumerate(stock_file):  # {
            pass
        # }
    # }

    return i + 1
# }

def append_to_last_col_csv(file, append, start_row, header):  # {
    """Appends to the last column of a csv file"""
    if path.isfile(file):  # {
        lines = []
        with open(file, "r") as f:  # {
            header = f.readline().replace('\n', '') + ",{h}\n".format(h=header)
            for i, line in enumerate(f):  # {
                new_line = line.replace('\n', '') + ",0\n" if i < start_row else "{l},{a}\n"\
                    .format(l=line.replace('\n', ''), a=append[i - start_row])
                lines.append(new_line)
            # }
        # }

        rm(file)

        with open(file, "w+") as f:  # {
            f.write(header)
            for line in lines:  # {
                f.write(line)
            # }
        # }
    # }
# }
