__author__ = 'Mark Provenzano'

import file_helper as fh

def rolling_avg_closing(file, span):  # {
    """Generator that gets the rolling average and displays one 'day' at a time."""

    n = 0
    stop = span
    ln_cnt = fh.line_count(file)
    file_in = [float(inp["close"]) for inp in stock_file_read(file)]

    while stop < ln_cnt:  # {
        temp = 0.0

        for i in range(n, stop):  # {
            temp += file_in[i]
        # }

        yield temp / span
        temp = 0.0
        n += 1
        stop += 1
    # }
# }

def stock_file_read(file):  # {
    """
    Reads the save_as file one line at a time and does not store it into memory
    :rtype : dictionary of stock values
    """

    with open(file) as sf:  # {

        # gets the header information from the downloaded temp_file
        headers = sf.readline().lower().replace('\n', '').split(',')

        # do...while loop that gets each line and presents them as a dictionary: key=header value=corresponding info
        while True:  # {
            line = sf.readline()

            # exits the loop if the line is null
            if not line:  # {
                break
            # }

            line = line.replace('\n', '').split(',')
            line_hash_table = {}

            for i in range(0, len(headers) - 1):  # {
                line_hash_table[headers[i]] = line[i]
            # }

            yield line_hash_table
        # }
    # }
# }

