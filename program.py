__author__ = 'Mark Provenzano'
# Created for the Florida Poly Data Lab

# region LIBRARy_IMPORTS

import stats
import file_helper as fh
from file_fetcher import FileFetcher as ff
from subprocess import Popen as po

# endregion

################################
#        yahoo url key         #
################################
# a = from month               #
# b = from day (2 digits)      #
# c = from year                #
# d = to month                 #
# e = to day (2 digits)        #
# f = to year                  #
################################

# region CLI_METHODS

prompt = "[~]> "

def prompt_entry():  # {
    """
    Prompts for the ticker of the desired stock.
    """

    global stock
    stock = input("Enter the stock ticker you would like to track\n{}".format(prompt)).upper()
    print("Looking up {}".format(stock))

    return stock
# }

def prompt_span():  # {
    """
    Prompts for the span of days.
    """

    return int(input("Enter in the number of days you want to span for the rolling average: "))
# }

# endregion

# region MAIN

def main():  # {
    stock = prompt_entry()

    file_f = ff(day=1, month=7, start_year=2015, end_year=2014, stock=stock)
    file_f.retrieve_file()

    for i in range(3):  # {
        span = prompt_span()
        x = stats.rolling_avg_closing(file_f.get_full_file(), span)
        fh.append_to_last_col_csv(file_f.get_full_file(), list(x), span, '{0} day'.format(span))
    # }

    # po(".\\{}".format(file_f.full_file))
# }

if __name__ == "__main__":  # {
    main()
# }

# endregion
